let modal = document.getElementById("video-1");
let modalBtn = document.querySelector('.about__play-wrapper');
let modalCls = document.getElementsByClassName("modal__close")[0];


const vid = document.querySelector('.modal__video');
modalBtn.onclick = function(e) {
    modal.style.display = "flex";
    document.querySelector('body').classList.add('lock')
    vid.src += '&autoplay=1';
    e.preventDefault();
}

modalCls.onclick = function() {
    modal.style.display = "none";
    vid.src = vid.getAttribute('src').replace('&autoplay=1', '');
    document.querySelector('body').classList.remove('lock');
}

window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
        vid.src = vid.getAttribute('src').replace('&autoplay=1', '');
        document.querySelector('body').classList.remove('lock');
    }

}