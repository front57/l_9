function openTab(ctx, tab) {
    let tabContent = document.getElementsByClassName("tabs__content");
    for (let i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }
    let tabLinks = document.getElementsByClassName("tabs__link");
    for (let i = 0; i < tabLinks.length; i++) {
        tabLinks[i].classList.remove('active');
    }
    document.getElementById(tab).style.display = "flex";
    ctx.classList.add('active');
}