const dropdownBtn = document.querySelector('.contacts__c-code-btn');
const list = document.querySelector('.contacts__code-list');
const currentCountry = document.querySelector('.contacts__country-field');
const hiddenCC = document.querySelector('.country__hidden-code');
var listMaxWidth;

list.addEventListener('click', function(e){
    dropdownToggle();
    currentCountry.innerHTML  = !e.target.classList.contains('contacts__code-item') ?
        e.target.closest('.contacts__code-item').innerHTML : e.target.innerHTML;
    hiddenCC.value = currentCountry.querySelector('.dropdown__country-code').innerText.replace(/^(\()|(\))$/g, '');

})

dropdownBtn.addEventListener('click', dropdownToggle);
window.addEventListener('resize', updateDropdownHeight);

function setListMaxWidth(){
    listMaxWidth =  window.innerWidth > 768 ? '400' : '100';
}

function dropdownToggle(){
    const dropdown = document.querySelector('.contacts__phone');
    dropdown.classList.toggle('active');
    if(dropdown.classList.contains('active')){
        if(window.innerWidth > 768){
            list.style.maxHeight = '400px';
        }else{
            list.style.maxHeight = '150px';
        }
    }else{
        list.style.maxHeight = null;
    }
}
function updateDropdownHeight(){
    if(document.querySelector('.contacts__phone').classList.contains('active')){
        if(window.innerWidth > 768){
            list.style.maxHeight = '400px';
        }else{
            list.style.maxHeight = '150px';
        }
    }
}