const imgs = document.querySelectorAll('.innoma__slide-img');

imgs.forEach(e => {
    const imgSrc = e.src;
    e.parentElement.style.backgroundImage = `url('${imgSrc}')`;
    e.parentElement.classList.add('background-center');
});

window.addEventListener('resize', changeEventImg);
window.addEventListener('load', changeEventImg);

function changeEventImg(){
    let eventsImg;
    if(window.innerWidth > 576){
        eventsImg = document.querySelectorAll('.events__img');
    }else{
        eventsImg = document.querySelectorAll('.events__img--vertical');
    }
    eventsImg.forEach(e => {
        const imgSrc = e.src;
        e.parentElement.style.backgroundImage = `url('${imgSrc}')`;
        e.parentElement.classList.add('background-center');
    });
}