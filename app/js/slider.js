$('.innoma__slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    dots: false,
    infinite: false,
    prevArrow: false,
    nextArrow: false,
    variableWidth:true,
    responsive: [
        {
            breakpoint: 1210,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: false,
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                centerMode: true,
            }
        },
    ]
});

$('.experts__slider').slick({
    slidesToShow: 4,
    slidesToScroll: 2,
    infinite:true,
    dots: false,
    //infinite: false,
    prevArrow: document.querySelector('.experts__prev-btn'),
    nextArrow: document.querySelector('.experts__next-btn'),
    variableWidth:false,
    //centerMode: true,
    responsive: [
        {
            breakpoint: 1054,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite:false,
                variableWidth:true,
                centerMode: false,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite:false,
                variableWidth:true,
            }
        },
        {
            breakpoint: 576,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite:false,
                variableWidth:true,
                centerMode: true,
            }
        },
    ]
});